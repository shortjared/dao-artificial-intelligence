// Player1.h: player 1 agent.
// Author:    Jared Short
// Date:	  April 10, 2012
// Version:   1.5
// MS Visual C++
#ifndef Player1_H
#define Player1_H
#include "state.h"
#include <vector>
#include <math.h>
using namespace std;

class Player1 : public Player
{
    private:
    public:
    int MaxPlayer,MinPlayer;
	Move get_move(unsigned short,board game_board);
    bool validmove(Move move, board);
    int isover(board);
    void get_tree_tier(vector<Move> &, board, unsigned short);
    void get_game_tree(vector<board> &);
    int evaluateboard(board, unsigned short);
};

Move Player1::get_move(unsigned short p, board game_board)
{
    MaxPlayer = p;
    MaxPlayer==1?MinPlayer=2:MinPlayer=1;
    vector<Move> moves;
    int bestindex;
    int alpha = -9999,alpha_new=-9999;

    //Current Board layout and moves
    board game_board_temp = game_board;
    get_tree_tier(moves,game_board_temp, p);

    //Find the best move
    for(int i=0; i<moves.size(); i++)
        {
            //Simulate move on temp board
            board game_board_temp = game_board;
            game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
            game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=p;

            //Finds if new move is better a loss
            alpha_new=max(-1000,evaluateboard(game_board_temp,p));

             //If it is better replace current best move
             if(alpha_new> alpha)
            {
             alpha = alpha_new;
             bestindex = i;
            }

            //If the two best moves are equal, replace with a relative probability
            //This is KEY in preventing loops.
            if(alpha_new == alpha && rand()%5 > moves.size()%5)
            {
                bestindex = i;
            }

        }
        	return moves[bestindex];
}


//Hueristics calculation for the board position and given player
int Player1::evaluateboard(board game_board,unsigned short p){
    board game_board_temp = game_board;

    if(isover(game_board)!=MaxPlayer)
    {
            //Define opposing player and get their moves
            int op;
            p==1?op=2:op=1;
            vector<Move> moves;
            get_tree_tier(moves,game_board, op);

            //If the opponent can win in next move return -hueristuc
            for(int i=0; i<moves.size(); i++)
                {
                    game_board_temp = game_board;
                    game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
                    game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=op;
                    if(isover(game_board_temp))
                    {
                     return -1000;
                    }
                }

        //This is where additional hueristics were intitally introduced.
        //Unfortunately the best resulting algorithm onyl returns a 0.
        //Absosoutely everything else was detrimental to the agents.
        game_board_temp = game_board;
        int score = 0;
        return score;


    }
    else
    {
        //Agent can win play this move
        return 1000;
    }
}


//Calculates all the legal moves in the tree tier for one player from
//The Given Board Position
void Player1::get_tree_tier(vector<Move> &moves , board game_board_original, unsigned short p)
{
    Move move_temp;
    move_temp.player=p;
    //Iterate through each square on the board
    for(int x=0;x < XYDIM;x++){
        for(int y=0;y< XYDIM; y++){
        if(game_board_original.layout[y][x] == p)
            {
                for(int xx=0;xx < XYDIM;xx++){
                    for(int yy=0;yy< XYDIM; yy++){
                        move_temp.src_x=x;
                        move_temp.src_y=y;
                        move_temp.dst_x=xx;
                        move_temp.dst_y=yy;
                        if(validmove(move_temp, game_board_original))
                         moves.push_back(move_temp);
                    }
                }
            }
        }
    }
}

bool Player1::validmove(Move move, board game_board_original)
{
	short x, y;
	board game_board;
	bool allclear;

	// Get current state of the game.
	game_board = game_board_original;

	// First, verify that a player isn't trying to move another's stone.
	if (move.player != game_board.layout[move.src_y][move.src_x])
		return false;

	// Second, verify that a player actually tries to move somewhere...
	if (move.dst_y == move.src_y && move.dst_x == move.src_x)  // Wow - really?
		return false;

	// Check for invalid 'North' move.
	if (move.dst_y < move.src_y && move.dst_x == move.src_x) {

		y = move.src_y - 1;
		allclear = true;

		while (allclear && y >= 0)
			if (game_board.layout[y][move.dst_x] != EMPTY)
				allclear = false;
			else
				y--;

		y++;	// Went past the valid move cell, so back it up.

		if (move.dst_y != y)
			return false;
	}

	// Check for invalid 'South' move.
	if (move.dst_y > move.src_y && move.dst_x == move.src_x) {

		y = move.src_y + 1;
		allclear = true;

		while (allclear && y < XYDIM)
			if (game_board.layout[y][move.dst_x] != EMPTY)
				allclear = false;
			else
				y++;

		y--;	// Went past the valid move cell, so back it up.

		if (move.dst_y != y)
			return false;
	}

	// Check for invalid 'East' move.
	if (move.dst_y == move.src_y && move.dst_x > move.src_x) {

		x = move.src_x + 1;
		allclear = true;

		while (allclear && x < XYDIM)
			if (game_board.layout[move.dst_y][x] != EMPTY)
				allclear = false;
			else
				x++;

		x--;	// Went past the valid move cell, so back it up.

		if (move.dst_x != x)
			return false;
	}

	// Check for invalid 'West' move.
	if (move.dst_y == move.src_y && move.dst_x < move.src_x) {

		x = move.src_x - 1;
		allclear = true;

		while (allclear && x >= 0)
			if (game_board.layout[move.dst_y][x] != EMPTY)
				allclear = false;
			else
				x--;

		x++;	// Went past the valid move cell, so back it up.

		if (move.dst_x != x)
			return false;
	}

	// Check for invalid 'Northeast' move.
	if (move.dst_y < move.src_y && move.dst_x > move.src_x) {

		x = move.src_x + 1;
		y = move.src_y - 1;
		allclear = true;

		while (allclear && y >= 0 && x < XYDIM)
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x++;
				y--;
			}

		x--;	// Went past the valid move cell, so back it up.
		y++;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	// Check for invalid 'Northwest' move.
	if (move.dst_y < move.src_y && move.dst_x < move.src_x) {

		x = move.src_x - 1;
		y = move.src_y - 1;
		allclear = true;

		while (allclear && y >= 0 && x >= 0)	// Fix due to Kirt Guthrie.
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x--;
				y--;
			}

		x++;	// Went past the valid move cell, so back it up.
		y++;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	// Check for invalid 'Southeast' move.
	if (move.dst_y > move.src_y && move.dst_x > move.src_x) {

		x = move.src_x + 1;
		y = move.src_y + 1;
		allclear = true;

		while (allclear && y < XYDIM && x < XYDIM)
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x++;
				y++;
			}

		x--;	// Went past the valid move cell, so back it up.
		y--;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	// Check for invalid 'Southwest' move.
	if (move.dst_y > move.src_y && move.dst_x < move.src_x) {

		x = move.src_x - 1;
		y = move.src_y + 1;
		allclear = true;

		while (allclear && y < XYDIM && x >= 0)
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x--;
				y++;
			}

		x++;	// Went past the valid move cell, so back it up.
		y--;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	return true;   // Made it past all the checks - Valid move!
}

int Player1::isover(board game_board)
{
	unsigned short x, y;
	int gameover = 0;

	// Check for a 'four corners' victory.
	if (game_board.layout[0][0] == game_board.layout[0][3] &&
		game_board.layout[0][0] == game_board.layout[3][0] &&
		game_board.layout[0][0] == game_board.layout[3][3] &&
		game_board.layout[0][0] != EMPTY) {

		gameover = game_board.layout[0][0];
		//winner = game_board.layout[0][0];

		return gameover;
	}

	// Check for a 'backdoor' victory.
	if (game_board.layout[0][0] != EMPTY && game_board.layout[0][1] != EMPTY &&
		game_board.layout[1][0] != EMPTY && game_board.layout[1][1] != EMPTY &&
		game_board.layout[0][0] != game_board.layout[0][1] &&
		game_board.layout[0][0] != game_board.layout[1][0] &&
		game_board.layout[0][0] != game_board.layout[1][1]) {

		gameover = game_board.layout[0][0];
		//winner = game_board.layout[0][0];

		return gameover;
	}
	if (game_board.layout[0][3] != EMPTY && game_board.layout[0][2] != EMPTY &&
		game_board.layout[1][3] != EMPTY && game_board.layout[1][2] != EMPTY &&
		game_board.layout[0][3] != game_board.layout[0][2] &&
		game_board.layout[0][3] != game_board.layout[1][3] &&
		game_board.layout[0][3] != game_board.layout[1][2]) {

		gameover = game_board.layout[0][3];
		//winner = game_board.layout[0][3];

		return gameover;
	}
	if (game_board.layout[3][0] != EMPTY && game_board.layout[3][1] != EMPTY &&
		game_board.layout[2][0] != EMPTY && game_board.layout[2][1] != EMPTY &&
		game_board.layout[3][0] != game_board.layout[3][1] &&
		game_board.layout[3][0] != game_board.layout[2][0] &&
		game_board.layout[3][0] != game_board.layout[2][1]) {

		gameover = game_board.layout[3][0];
		//winner = game_board.layout[3][0];

		return gameover;
	}
	if (game_board.layout[3][3] != EMPTY && game_board.layout[3][2] != EMPTY &&
		game_board.layout[2][3] != EMPTY && game_board.layout[2][2] != EMPTY &&
		game_board.layout[3][3] != game_board.layout[3][2] &&
		game_board.layout[3][3] != game_board.layout[2][3] &&
		game_board.layout[3][3] != game_board.layout[2][2]) {

		gameover = game_board.layout[3][3];
		//winner = game_board.layout[3][3];

		return gameover;
	}

	// Check for 'column' victory.
	for (x = 0; x < XYDIM; x++)
		if (game_board.layout[0][x] != EMPTY && game_board.layout[1][x] == game_board.layout[0][x]
											 && game_board.layout[2][x] == game_board.layout[0][x]
											 && game_board.layout[3][x] == game_board.layout[0][x]) {
			gameover = game_board.layout[0][x];
			//winner = game_board.layout[0][x];

			return gameover;
		}

	// Check for 'row' victory.
	for (y = 0; y < XYDIM; y++)
		if (game_board.layout[y][0] != EMPTY && game_board.layout[y][1] == game_board.layout[y][0]
											 && game_board.layout[y][2] == game_board.layout[y][0]
											 && game_board.layout[y][3] == game_board.layout[y][0]) {
			gameover = game_board.layout[y][0];
			//winner = game_board.layout[y][0];

			return gameover;
		}

	// Check for 'square cluster' victory.
	for (y = 0; y < XYDIM - 1; y++)
		for (x = 0; x < XYDIM - 1; x++)
			if (game_board.layout[y][x] != EMPTY && game_board.layout[y][x] == game_board.layout[y+1][x]
												 && game_board.layout[y][x] == game_board.layout[y][x+1]
												 && game_board.layout[y][x] == game_board.layout[y+1][x+1]) {
				gameover = game_board.layout[y][x];
				//winner = game_board.layout[y][x];

				return gameover;
			}

	return gameover;
}

#endif
