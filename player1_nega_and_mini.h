// player1.h: player 1 agent.
// Author:    Jared Short
// Date:	  April 10, 2012
// Version:   1.5
// MS Visual C++
#ifndef PLAYER1_H
#define PLAYER1_H
#include "state.h"
#include <vector>
using namespace std;

class Player1 : public Player
{
    private:
    public:
    int MaxPlayer,MinPlayer;
    bool validmove(Move move, board);
    int isover(board);
    void get_tree_tier(vector<Move> &, board, unsigned short);
    Move get_move(unsigned short p, board game_board);
    Move minimax(board,int);
    int negamax(board, int);
    int maxmove(board, int);
    int minmove(board, int);
};

Move Player1::get_move(unsigned short p, board game_board)
{
    MaxPlayer = p;
    MaxPlayer==1?MinPlayer=2:MinPlayer=1;
    int depth=5;

    //Current board layout
    board game_board_temp = game_board;

    /*
        //NEGAMAX USAGE CODE
        Move bestmove;
        int alpha = -9999;
        int new_alpha;
        vector<Move> moves;
        get_tree_tier(moves,game_board,MaxPlayer);
        bestmove = moves[0];
        for(int i=0;i<moves.size();i++)
        {
            board game_board_temp = game_board;
            game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
            game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=MaxPlayer;
            new_alpha = negamax(game_board_temp,depth);
             cout << "Move: " <<  moves[i].src_x <<  moves[i].src_y <<  moves[i].dst_x <<  moves[i].dst_y << "\n";
             cout << "Alpha: " << new_alpha << "\n";
            if(new_alpha>alpha)
            {
             bestmove = moves[i];
             alpha = new_alpha;
            }
        }
    //Minimax to find best move
    return bestmove;
    */

    return minimax(game_board,depth);

}

//NegaMax
int Player1::negamax(board game_board, int depth)
{
    if(isover(game_board)!=0)
        return 1000;
    if(depth <= 0)
        return 0;

    int alpha = -9999;
    vector<Move> moves;
    get_tree_tier(moves,game_board,MaxPlayer);
    for(int i=0;i<moves.size();i++)
        {
            board game_board_temp = game_board;
            game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
            game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=MaxPlayer;
            alpha = max(alpha,-negamax(game_board_temp,depth-1));
        }

    return alpha;
}
//Minimax starts with max move find
Move Player1::minimax(board game_board, int depth)
{
        Move bestmove;
        int alpha = -9999;
        int new_alpha;
        vector<Move> moves;
        get_tree_tier(moves,game_board,MaxPlayer);
        bestmove = moves[0];
        for(int i=0;i<moves.size();i++)
        {
            board game_board_temp = game_board;
            game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
            game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=MaxPlayer;
            new_alpha = maxmove(game_board_temp,depth-1);
             cout << "Move: " <<  moves[i].src_x <<  moves[i].src_y <<  moves[i].dst_x <<  moves[i].dst_y << "\n";
             cout << "Alpha: " << new_alpha << "\n";
            if(new_alpha>alpha)
            {
             bestmove = moves[i];
             alpha = new_alpha;
            }
        }

        return bestmove;
}

//Find move for MaxPlayer
int Player1::maxmove(board game_board, int depth)
{
    int isoverscore = isover(game_board); //Stores value of gameboard, improves runtime
    if(isoverscore==MaxPlayer) //If the game is over because of win return 1000
        return 1000;
    else if(isoverscore==MinPlayer)
        return -1000;
    else if(depth <= 0) //If the depth has been reached, ambigous return
    {
        return 0;
    }
    else
    {
        Move bestmove;
        int alpha = -9999;
        vector<Move> moves;
        get_tree_tier(moves,game_board,MaxPlayer);
        bestmove = moves[0];
        for(int i=0;i<moves.size();i++)
        {
            board game_board_temp = game_board;
            game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
            game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=MaxPlayer;
            alpha = max(alpha,minmove(game_board_temp,depth-1));
        }
        return alpha;
    }
}

//Find move for MinPlayer
int Player1::minmove(board game_board, int depth)
{
    int isoverscore = isover(game_board); //Stores value of gameboard, improves runtime
    if(isoverscore==MaxPlayer) //If the game is over because of win return 1000
        return 1000;
    else if(isoverscore==MinPlayer)
        return -1000;
    else if(depth <= 0) //If the depth has been reached, ambigous return
    {
        return 0;
    }
    else
    {
        Move bestmove;
        int alpha = 9999;
        vector<Move> moves;
        get_tree_tier(moves,game_board,MinPlayer);
        bestmove = moves[0];
        for(int i=0;i<moves.size();i++)
        {
            board game_board_temp = game_board;
            game_board_temp.layout[moves[i].src_y][moves[i].src_x]=0;
            game_board_temp.layout[moves[i].dst_y][moves[i].dst_x]=MinPlayer;
            alpha = min(alpha,maxmove(game_board_temp,depth-1));
        }
        return alpha;
    }
}

//Calculates all the legal moves in the tree tier for one player from
//The Given Board Position
void Player1::get_tree_tier(vector<Move> &moves , board game_board_original, unsigned short p)
{
    Move move_temp;
    move_temp.player=p;
    //Iterate through each square on the board
    for(int x=0;x < XYDIM;x++){
        for(int y=0;y< XYDIM; y++){
        if(game_board_original.layout[y][x] == p)
            {
                for(int xx=0;xx < XYDIM;xx++){
                    for(int yy=0;yy< XYDIM; yy++){
                        move_temp.src_x=x;
                        move_temp.src_y=y;
                        move_temp.dst_x=xx;
                        move_temp.dst_y=yy;
                        if(validmove(move_temp, game_board_original))
                         moves.push_back(move_temp);
                    }
                }
            }
        }
    }
}

bool Player1::validmove(Move move, board game_board_original)
{
	short x, y;
	board game_board;
	bool allclear;

	// Get current state of the game.
	game_board = game_board_original;

	// First, verify that a player isn't trying to move another's stone.
	if (move.player != game_board.layout[move.src_y][move.src_x])
		return false;

	// Second, verify that a player actually tries to move somewhere...
	if (move.dst_y == move.src_y && move.dst_x == move.src_x)  // Wow - really?
		return false;

	// Check for invalid 'North' move.
	if (move.dst_y < move.src_y && move.dst_x == move.src_x) {

		y = move.src_y - 1;
		allclear = true;

		while (allclear && y >= 0)
			if (game_board.layout[y][move.dst_x] != EMPTY)
				allclear = false;
			else
				y--;

		y++;	// Went past the valid move cell, so back it up.

		if (move.dst_y != y)
			return false;
	}

	// Check for invalid 'South' move.
	if (move.dst_y > move.src_y && move.dst_x == move.src_x) {

		y = move.src_y + 1;
		allclear = true;

		while (allclear && y < XYDIM)
			if (game_board.layout[y][move.dst_x] != EMPTY)
				allclear = false;
			else
				y++;

		y--;	// Went past the valid move cell, so back it up.

		if (move.dst_y != y)
			return false;
	}

	// Check for invalid 'East' move.
	if (move.dst_y == move.src_y && move.dst_x > move.src_x) {

		x = move.src_x + 1;
		allclear = true;

		while (allclear && x < XYDIM)
			if (game_board.layout[move.dst_y][x] != EMPTY)
				allclear = false;
			else
				x++;

		x--;	// Went past the valid move cell, so back it up.

		if (move.dst_x != x)
			return false;
	}

	// Check for invalid 'West' move.
	if (move.dst_y == move.src_y && move.dst_x < move.src_x) {

		x = move.src_x - 1;
		allclear = true;

		while (allclear && x >= 0)
			if (game_board.layout[move.dst_y][x] != EMPTY)
				allclear = false;
			else
				x--;

		x++;	// Went past the valid move cell, so back it up.

		if (move.dst_x != x)
			return false;
	}

	// Check for invalid 'Northeast' move.
	if (move.dst_y < move.src_y && move.dst_x > move.src_x) {

		x = move.src_x + 1;
		y = move.src_y - 1;
		allclear = true;

		while (allclear && y >= 0 && x < XYDIM)
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x++;
				y--;
			}

		x--;	// Went past the valid move cell, so back it up.
		y++;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	// Check for invalid 'Northwest' move.
	if (move.dst_y < move.src_y && move.dst_x < move.src_x) {

		x = move.src_x - 1;
		y = move.src_y - 1;
		allclear = true;

		while (allclear && y >= 0 && x >= 0)	// Fix due to Kirt Guthrie.
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x--;
				y--;
			}

		x++;	// Went past the valid move cell, so back it up.
		y++;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	// Check for invalid 'Southeast' move.
	if (move.dst_y > move.src_y && move.dst_x > move.src_x) {

		x = move.src_x + 1;
		y = move.src_y + 1;
		allclear = true;

		while (allclear && y < XYDIM && x < XYDIM)
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x++;
				y++;
			}

		x--;	// Went past the valid move cell, so back it up.
		y--;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	// Check for invalid 'Southwest' move.
	if (move.dst_y > move.src_y && move.dst_x < move.src_x) {

		x = move.src_x - 1;
		y = move.src_y + 1;
		allclear = true;

		while (allclear && y < XYDIM && x >= 0)
			if (game_board.layout[y][x] != EMPTY)
				allclear = false;
			else {
				x--;
				y++;
			}

		x++;	// Went past the valid move cell, so back it up.
		y--;

		if (move.dst_x != x || move.dst_y != y)
			return false;
	}

	return true;   // Made it past all the checks - Valid move!
}

int Player1::isover(board game_board)
{
	unsigned short x, y;
	int gameover = 0;

	// Check for a 'four corners' victory.
	if (game_board.layout[0][0] == game_board.layout[0][3] &&
		game_board.layout[0][0] == game_board.layout[3][0] &&
		game_board.layout[0][0] == game_board.layout[3][3] &&
		game_board.layout[0][0] != EMPTY) {

		gameover = game_board.layout[0][0];
		//winner = game_board.layout[0][0];

		return gameover;
	}

	// Check for a 'backdoor' victory.
	if (game_board.layout[0][0] != EMPTY && game_board.layout[0][1] != EMPTY &&
		game_board.layout[1][0] != EMPTY && game_board.layout[1][1] != EMPTY &&
		game_board.layout[0][0] != game_board.layout[0][1] &&
		game_board.layout[0][0] != game_board.layout[1][0] &&
		game_board.layout[0][0] != game_board.layout[1][1]) {

		gameover = game_board.layout[0][0];
		//winner = game_board.layout[0][0];

		return gameover;
	}
	if (game_board.layout[0][3] != EMPTY && game_board.layout[0][2] != EMPTY &&
		game_board.layout[1][3] != EMPTY && game_board.layout[1][2] != EMPTY &&
		game_board.layout[0][3] != game_board.layout[0][2] &&
		game_board.layout[0][3] != game_board.layout[1][3] &&
		game_board.layout[0][3] != game_board.layout[1][2]) {

		gameover = game_board.layout[0][3];
		//winner = game_board.layout[0][3];

		return gameover;
	}
	if (game_board.layout[3][0] != EMPTY && game_board.layout[3][1] != EMPTY &&
		game_board.layout[2][0] != EMPTY && game_board.layout[2][1] != EMPTY &&
		game_board.layout[3][0] != game_board.layout[3][1] &&
		game_board.layout[3][0] != game_board.layout[2][0] &&
		game_board.layout[3][0] != game_board.layout[2][1]) {

		gameover = game_board.layout[3][0];
		//winner = game_board.layout[3][0];

		return gameover;
	}
	if (game_board.layout[3][3] != EMPTY && game_board.layout[3][2] != EMPTY &&
		game_board.layout[2][3] != EMPTY && game_board.layout[2][2] != EMPTY &&
		game_board.layout[3][3] != game_board.layout[3][2] &&
		game_board.layout[3][3] != game_board.layout[2][3] &&
		game_board.layout[3][3] != game_board.layout[2][2]) {

		gameover = game_board.layout[3][3];
		//winner = game_board.layout[3][3];

		return gameover;
	}

	// Check for 'column' victory.
	for (x = 0; x < XYDIM; x++)
		if (game_board.layout[0][x] != EMPTY && game_board.layout[1][x] == game_board.layout[0][x]
											 && game_board.layout[2][x] == game_board.layout[0][x]
											 && game_board.layout[3][x] == game_board.layout[0][x]) {
			gameover = game_board.layout[0][x];
			//winner = game_board.layout[0][x];

			return gameover;
		}

	// Check for 'row' victory.
	for (y = 0; y < XYDIM; y++)
		if (game_board.layout[y][0] != EMPTY && game_board.layout[y][1] == game_board.layout[y][0]
											 && game_board.layout[y][2] == game_board.layout[y][0]
											 && game_board.layout[y][3] == game_board.layout[y][0]) {
			gameover = game_board.layout[y][0];
			//winner = game_board.layout[y][0];

			return gameover;
		}

	// Check for 'square cluster' victory.
	for (y = 0; y < XYDIM - 1; y++)
		for (x = 0; x < XYDIM - 1; x++)
			if (game_board.layout[y][x] != EMPTY && game_board.layout[y][x] == game_board.layout[y+1][x]
												 && game_board.layout[y][x] == game_board.layout[y][x+1]
												 && game_board.layout[y][x] == game_board.layout[y+1][x+1]) {
				gameover = game_board.layout[y][x];
				//winner = game_board.layout[y][x];

				return gameover;
			}

	return gameover;
}

#endif
